var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var records = require('./record.js');
app.get('/', function(req, res){
	res.sendFile(__dirname + '/index.html');
});

var OnlineCount = 0;

io.on('connection', function(socket){
	OnlineCount += 1;
	console.log('User Connect.');
	io.emit('online', OnlineCount);
	io.emit('max record', records.getMax());
	//io.emit('chat record', records.get());
	function mcallback(msgs){
		socket.emit('chat record', msgs);
	}
	records.get(mcallback);
	socket.on('disconnect', function(){
		console.log('User Disconnect.');
		OnlineCount = (OnlineCount < 0) ? 0 : OnlineCount -= 1;
		io.emit('disconnect', OnlineCount);
	});
	socket.on('greet', function(){
		socket.emit('greet', OnlineCount);
	});

	socket.on('send', function(msg){
		console.log(msg);
		//socket.emit('msg', msg);
		records.push(msg);
	});

});
records.on('new_message', function(msg){
	io.emit('msg', msg);
})
server.listen(3000, function(){
	console.log('Server Running.');
});